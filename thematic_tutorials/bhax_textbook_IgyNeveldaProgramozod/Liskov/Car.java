public class Car
{

  String Make;
  String Type;
  String Color;
 

  public String getMake()
  {
    return Make;
  }
 
  public String getType()
  {
    return Type;
  }
 
  public String getColor()
  {
    return Color;
  }
 

  public Car(String Make, String Type, String Color)
  {
    this.Make = Make;
    this.Type = Type;
    this.Color = Color;
  }

}