// Abstract class
abstract class Golf {
    // Abstract method (does not have a body)
    public abstract void zeroToSixty();
    // Regular method
    public void serviceDue(int kilometers) {
      System.out.println("Kilometers till next service: "+(100000 - kilometers));
    }
  }
  
  // Subclass (inherit from Animal)
  class Gfj extends Golf {
    public void zeroToSixty() {
      System.out.println("11.2 Sec");
    }
  }
  
  class Cars {
    public static void main(String[] args) {
      Gfj golf1 = new Gfj(); // Create a Pig object
      golf1.zeroToSixty();
      golf1.serviceDue(72000);
    }
  }