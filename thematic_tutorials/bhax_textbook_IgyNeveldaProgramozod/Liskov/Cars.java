
abstract class Golf {
   
    public abstract void zeroToSixty();
  
    public void serviceDue(int kilometers) {
      System.out.println("Kilometers till next service: "+(100000 - kilometers));
    }
  }
  
  
  class Gfj extends Golf {
    public void zeroToSixty() {
      System.out.println("11.2 Sec");
    }
  }
  
  public class Cars {
    public static void main(String[] args) {
      Gfj golf1 = new Gfj(); 
      golf1.zeroToSixty();
      golf1.serviceDue(72000);
    }
  }