package Golfs;

abstract class Golf {
    public void serviceDue(final int kilometers) {
        System.out.println("Kilometers till service: " + (100000 - kilometers));
    }

    public abstract void zeroToSixty();

    public abstract void engineType();
}

class gfj extends Golf {

    @Override
    public void zeroToSixty() {

        System.out.println("11.2 Sec");
    }

    @Override
    public void engineType() {

        System.out.println("1.6 Liter");
    }
}

class ian extends Golf {

    @Override
    public void zeroToSixty() {

        System.out.println("7.2 Sec");
    }

    @Override
    public void engineType() {

        System.out.println("2.8 Liter");
    }



    public static void main(String[] args) {

        Golf golf1 = new gfj();
        Golf golf2 = new ian();

        golf1.zeroToSixty();
        golf1.engineType();
        golf1.serviceDue(78000);

        golf2.zeroToSixty();
        golf2.engineType();
        golf2.serviceDue(98000);
    }

