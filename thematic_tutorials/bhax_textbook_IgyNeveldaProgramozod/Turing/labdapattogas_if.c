#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int main(void)
{
    WINDOW *ablak;
    ablak = initscr();

    int x = 0;
    int y = 0;

    int xkoordinata = 1;
    int ykoordinata = 1;

    int xoszlop;
    int yoszlop;

    for(;;)
    {
        getmaxyx(ablak, yoszlop, xoszlop);

        mvprintw(y, x, "O");

        refresh();
        usleep(100000);

        x = x + xkoordinata;
        y = y + ykoordinata;

        if(x >= xoszlop-1)
        {
            xkoordinata = xkoordinata * -1;
        }

        if(x <= 0)
        {
            xkoordinata = xkoordinata * -1;
        }

        if(y <= 0)
        {
            ykoordinata = ykoordinata * -1;
        }

        if(y >= yoszlop-1)
        {
            ykoordinata = ykoordinata * -1;
        }

    }

    return 0;
}