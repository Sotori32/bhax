library(matlab)

stp <- function(x)
{
    primek = primes(x)
    kulonbseg = primek[2:length(primek)]-primek[1:length(primek)-1]
    egymasutan_kulonbseg = which(kulonbseg==2)
    a1primek = primek[egymasutan_kulonbseg]
    a2primek = primek[egymasutan_kulonbseg]+2
    a1esa2 = 1/a1primek+1/a2primek
    return(sum(a1esa2))
}

x=seq(13, 1000000, by=10000)
y=sapply(x, FUN = stp)
plot(x,y,type="b")