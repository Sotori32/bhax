#include <stdio.h>

int main()
{
	int a;
	int b;
	printf("Add meg 'a' és 'b' értékét (szóközzel válaszd őket el egymástól, majd nyomj egy Enter-t)\n");
	scanf("%d %d", &a, &b);

	a = a + b;
	b = a - b;
	a = a - b;

	printf("Cserélt 'a': %d\nCserélt 'b': %d\n", a, b);

	return 0;
}