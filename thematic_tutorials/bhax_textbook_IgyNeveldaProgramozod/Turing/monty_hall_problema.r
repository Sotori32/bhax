probalkozasok_szama = 10000000
probalkozas = sample(1:3, probalkozasok_szama, replace=T)
jatekos = sample(1:3, probalkozasok_szama, replace=T)
rozsa_gyorgy = vector(length = probalkozasok_szama)

for (i in 1:probalkozasok_szama) {

    if(probalkozas[i]==jatekos[i]){
    
        ajtok=setdiff(c(1,2,3), probalkozas[i])
    
    }else{
    
        ajtok=setdiff(c(1,2,3), c(probalkozas[i], jatekos[i]))
    
    }

    rozsa_gyorgy[i] = ajtok[sample(1:length(ajtok),1)]

}

nem_valt_megnyeri = which(probalkozas==jatekos)

valt = vector(length = probalkozasok_szama)

for (i in 1:probalkozasok_szama) {

    mikor_valt = setdiff(c(1,2,3), c(rozsa_gyorgy[i], jatekos[i]))
    valt[i] = mikor_valt[sample(1:length(mikor_valt),1)]
    
}

valt_megnyeri = which(probalkozas==valt)


sprintf("Probalkozasok szama: %i", probalkozasok_szama)
sprintf("Ha nem valtoztat es megnyeri:")
length(nem_valt_megnyeri)
sprintf("Ha valtoztat es megnyeri:")
length(valt_megnyeri)
sprintf("Valtoztatas nelkuli nyeres aranya a valtoztatasos nyereshez:")
length(nem_valt_megnyeri)/length(valt_megnyeri)
sprintf("Valtoztatas nelkuli nyeres es valtoztatsos nyeres osszege:")
length(nem_valt_megnyeri)+length(valt_megnyeri)