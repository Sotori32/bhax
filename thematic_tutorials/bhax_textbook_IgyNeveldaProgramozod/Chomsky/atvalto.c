#include <stdio.h>

int main(){
	int a, db = 0;
	printf("Írj egy decimális számot!\n");
	scanf("%d", &a);
	printf("Unárisba átváltva:\n");
	for (int i = 0; i < a; i++)
	{
		printf("I");
		db++;
		if (db % 5 == 0) 
        {
            printf(" ");
        }
	}
	printf("\n");
	return 0;
}

