#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
	srand(time(NULL));
	int t [10], i=0;
	while(i!=10)
	{
		
		t[i++]=rand() % 101;
		printf("%d ", t[i-1]);
	}
	printf("\n");
	
	int min=t[0], max=t[0]; //C99-től lehet a main()-en belül bármikor változót deklarálni, illetve a C++ tipikus kommentelés is csak innenstől lehetséges//
	for(i=0;i<10;i++)
	{
		if(t[i]<min)
			min=t[i];
		if(t[i]>max)
			max=t[i];
	}
	printf("A legkisebb: %d, a legnagyobb: %d\n", min, max);
}